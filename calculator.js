export class Calculator {
	static memory = Number.NaN;
	static actualString = '0';
	static previousString = '';
	static actualValue = Number.NaN;
	static previousValue = Number.NaN;
	static lockbs = false;
	static previousOp = '';
	static memStatus = false;

	static handleOp(op) {
		console.log(op);
		if (op === '<') {
			if (!this.lockbs) this.popDigit();
		} else if (op === '%') {
		} else if (op === 'CE') {
			this.resetPrimary();
		} else if (op === 'C') {
			this.reset();
		} else if (op === '=') {
			if (this.previousValue !== Number.NaN) {
				let parsedFloat = Number.parseFloat(this.actualString);
				if (this.previousOp === '/' && parsedFloat == 0) {
					this.actualString = 'Error';
				} else {
					if (this.previousOp === '/') {
						this.previousString = `${this.previousString}${this.actualString}=`;
						this.actualValue = this.previousValue / parsedFloat;
						this.actualString = this.actualValue.toString();
					} else if (this.previousOp === '*') {
                        this.previousString = `${this.previousString}${this.actualString}=`;
						this.actualValue = this.previousValue * parsedFloat;
						this.actualString = this.actualValue.toString();
					} else if (this.previousOp === '-') {
						this.previousString = `${this.previousString}${this.actualString}=`;
						this.actualValue = this.previousValue - parsedFloat;
						this.actualString = this.actualValue.toString();
					} else if (this.previousOp === '+') {
						this.previousString = `${this.previousString}${this.actualString}=`;
						this.actualValue = this.previousValue + parsedFloat;
						this.actualString = this.actualValue.toString();
					}
				}
				this.lockbs = true;
			}
		} else if (op === '&') {
			if (this.actualString === '0') {
				this.actualString = 'Syntax Error';
			} else {
				this.previousString = `1/(${this.actualString})`;
				this.actualValue = 1.0 / Number.parseFloat(this.actualString);
				this.actualString = this.actualValue.toString();
			}
			this.lockbs = true;
		} else if (op === '^') {
			let parsedFloat = Number.parseFloat(this.actualString);
			this.previousString = `sqr(${this.actualString})`;
			this.actualValue = parsedFloat * parsedFloat;
			this.actualString = this.actualValue.toString();
		} else if (op === '¬') {
			let parsedFloat = Number.parseFloat(this.actualString);
			this.previousString = `sqrt(${this.actualString})`;
			this.actualValue = Math.sqrt(parsedFloat);
			this.actualString = this.actualValue.toString();
		} else if (op === '/') {
			this.previousValue = Number.parseFloat(this.actualString);
			this.previousString = `${this.previousValue}/`;
			this.actualValue = 0;
			this.actualString = '0';
			this.previousOp = '/';
		} else if (op === '*') {
			this.previousValue = Number.parseFloat(this.actualString);
			this.previousString = `${this.previousValue}x`;
			this.actualValue = 0;
			this.actualString = '0';
			this.previousOp = '*';
		} else if (op === '-') {
			this.previousValue = Number.parseFloat(this.actualString);
			this.previousString = `${this.previousValue}-`;
			this.actualValue = 0;
			this.actualString = '0';
			this.previousOp = '-';
		} else if (op === '+') {
			this.previousValue = Number.parseFloat(this.actualString);
			this.previousString = `${this.previousValue}+`;
			this.actualValue = 0;
			this.actualString = '0';
			this.previousOp = '+';
		} else if (op === '.') {
			if(!this.actualString.includes('.')){
				this.actualString = this.actualString + '.';
			}
		} else if (op === '!') {
			let actualNum = Number.parseFloat(this.actualString);
			this.actualString = (-1*actualNum).toString();
		} 
	}

	static handleMop(mop) {
		if(mop === 'MC') {
			this.memory = Number.NaN;
			this.memStatus = false;
		} else if (mop === 'MR') {
			if(this.memStatus) {
				this.actualString = this.memory.toString();
			}
		} else if (mop === 'M+') {
			if(this.memory !== Number.NaN){
				this.memory += Number.parseFloat(this.actualString);
			}
			else {
				this.handleMop('MS');
			}
		} else if (mop === 'M-') {
			if(this.memory !== Number.NaN){
				this.memory -= Number.parseFloat(this.actualString);
			} else {
				this.handleMop('MS');
			}
		} else if (mop === 'MS') {
			this.memory = Number.parseFloat(this.actualString);
			this.memStatus = true;
		}
	}

	static pushDigit(strDigit) {
		if (this.actualString === '0' || this.lockbs) {
			if (strDigit != '0') this.actualString = strDigit;
		} else {
			this.actualString = this.actualString + strDigit;
		}
		this.lockbs = false;
	}

	static reset() {
		this.actualString = '0';
		this.previousString = '';
		this.actualValue = Number.NaN;
		this.previousValue = Number.NaN;
	}

	static resetPrimary() {
		this.actualString = '0';
		this.actualValue = Number.NaN;
	}

	static popDigit() {
		if (this.actualString.length == 1) {
			this.actualString = '0';
		} else {
			this.actualString = this.actualString.substring(
				0,
				this.actualString.length - 1
			);
		}
	}

	static refreshMainDisplay(dispElem) {
		dispElem.innerHTML = this.actualString;
	}

	static refreshDisplays(mainDisplayElem, secDisplayElem) {
		mainDisplayElem.innerHTML = this.actualString;
		secDisplayElem.innerHTML = this.previousString;
	}
}
