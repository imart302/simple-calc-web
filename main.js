import { Calculator } from './calculator';

export function init() {
	console.log('init');

	const digitmap = new Map();
	digitmap.set('0', document.getElementById('zero_btn'));
	digitmap.set('1', document.getElementById('one_btn'));
	digitmap.set('2', document.getElementById('two_btn'));
	digitmap.set('3', document.getElementById('three_btn'));
	digitmap.set('4', document.getElementById('four_btn'));
	digitmap.set('5', document.getElementById('five_btn'));
	digitmap.set('6', document.getElementById('six_btn'));
	digitmap.set('7', document.getElementById('seven_btn'));
	digitmap.set('8', document.getElementById('eigth_btn'));
	digitmap.set('9', document.getElementById('nine_btn'));

	const primDisplay = document.getElementById('primary_display_text');
	const secDisplay = document.getElementById('secondary_display_text');

	for (const [num, element] of digitmap.entries()) {
		element.addEventListener('click', (ev) => {
			Calculator.pushDigit(num);
			Calculator.refreshMainDisplay(primDisplay);
		});
	}

	const opmap = new Map();
	opmap.set('%', document.getElementById('percent'));
	opmap.set('CE', document.getElementById('ce'));
	opmap.set('C', document.getElementById('C'));
	opmap.set('<', document.getElementById('backspace'));
	opmap.set('&', document.getElementById('invd'));
	opmap.set('^', document.getElementById('square'));
	opmap.set('¬', document.getElementById('sqrt'));
	opmap.set('/', document.getElementById('div'));
	opmap.set('*', document.getElementById('mul'));
	opmap.set('-', document.getElementById('sub'));
	opmap.set('+', document.getElementById('add'));
	opmap.set('!', document.getElementById('invs'));
	opmap.set('.', document.getElementById('dec'));
	opmap.set('=', document.getElementById('calc'));

	for (const [op, element] of opmap) {
		element.addEventListener('click', (ev) => {
			Calculator.handleOp(op);
			Calculator.refreshDisplays(primDisplay, secDisplay);
		});
	}

	const memElement = document.getElementById('memory_icon');
	const memmap = new Map();
	
	memmap.set('MC', document.getElementById('memclear'));
	memmap.set('MR', document.getElementById('memget'));
	memmap.set('M+', document.getElementById('memadd'));
	memmap.set('M-', document.getElementById('memsub'));
	memmap.set('MS', document.getElementById('memset'));

	for (const [mop, element] of memmap) {
		element.addEventListener('click', (ev) => {
			Calculator.handleMop(mop);
			if(Calculator.memStatus){
				memmap.get('MC').disabled = false;
				memmap.get('MR').disabled = false;
				memElement.innerHTML = 'M'
			} else {
				memmap.get('MC').disabled = true;
				memmap.get('MR').disabled = true
				memElement.innerHTML = '';
			}
			Calculator.refreshMainDisplay(primDisplay);
		});
	}
}
